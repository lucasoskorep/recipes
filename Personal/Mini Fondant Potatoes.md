![fondant-potatoes](../Images/mini-fondant-potatoes.jpg)

**Ingredients:**
- 1.5 - 2 pounds mini Potatoes 
- 2 tablespoons of butter
- 2 cups of stock 
	- Chicken or veggie preferred
	- If using beef stock I would just add some water as the stronger flavor could easily overwhelm the potatoes while steaming in the stock
- Thyme - 1 sprig to the stock
- 1 quarter large onion, or 1/2 medium sized onion, finely minced
- Salt, pepper, and cayenne

**Note:**
- This is meant to be a base for the fondant potatoes.  Lots of fun to be had experimenting with different herb+spice mixes in the stock and sauce.  Just be warned that the chicken flavor can be fairly strong in the end so pick and adjust accordingly!  

**Steps:**
- Wash potatoes
- Cut top and bottom off each potato so that they are all ~1.5 cm or .5 in 
- Heat the butter in pan over medium-high heat
- Fry one side of the potatoes on medium-high for 10 minutes
- Flip each potato and fry for 3 minutes
- De-glaze pan with stock and add the minced onions, and thyme
- Cover with lid and reduce heat to low such that the stock is barely bubbling for 10 minutes
- Uncover and optionally remove the thyme now if you don't want it to be too strong
- Return to medium high heat to reduce the stock 
	- Reduction should leave a thick/visible coating onto the back of a spoon or on top of a potato.  Picture attached as an example(see picture) ![fondant-potatoes](../Images/fondant-sauce.png)
- Salt and pepper and cayenne to taste
	- If bouillon was used for chicken stock be careful with salt here as there is probably already enough salt from it. 


![fondant-with-thighs](../Images/fondant-spinach-slow-thighs.jpg)
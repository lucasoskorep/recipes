Ingredients
- 1 pound of sprouts
- 3 Tbsp EVOO
- Salt and pepper

For the Glaze:
- 1 Tbsp miso paste or 2 Tbsp condensed liquid miso 
- 2 Tbsp rice vinegar
- 1 Tbsp tamari
- 1-1.5 Tbsp real maple syrup

Glaze:
1. Add miso, vinegar, tamari and syrup to a bowl
2. Stir and chill

Cooking the Sprouts:
1. Preheat oven to 425
2. Remove stems from sprouts
3. Cut them each in half and add them all to a bowl
4. Add EVOO, salt and pepper and toss the sprouts
6. Pan to medium-high heat
7. Place sprouts cut side down and cook for 3-4 minutes
8. Transfer to baking tray ensure they are not crowded
9. Bake for 13 minutes
10. Remove from  Oven, return to pan at medium heat
11. Add .5-.75 of the glaze and toss the sprouts while cooking for 2-3 minutes
12. Remove from pan and put in serving tray
13. Serve with remaining sauce for dipping/extra as wanted

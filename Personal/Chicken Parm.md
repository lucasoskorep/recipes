Sauce:

- 16 ounces of Marinara as a base
- 1 tablespoon Balsamic vinegar
- 1-2 teaspoons garlic powder or roughly that amount of roasted minced garlic
- 1 teaspoon Italian seasoning, parsley, basil, oregano
- 1/2 - 1 teaspoon red pepper flakes depending on spice level

Balsamic vinegar is one of the key ingredients of Chicken Parm that is not included in marinara add this regardless.  Everything else is dependent on what marinara you use, but generally you want a more flavorful sauce and to use slightly less of the sauce for this dish.

For these measurements I used this as a base - https://cookieandkate.com/simple-marinara-sauce-recipe/, but have also used a couple canned marinaras with not many if any changes.

For the sauce add it all together, bring to a low boil, and let it sit for like 15- 20 minutes reducing into a slightly thicker sauce than normal.  If you made your own marinara here it may be a bit watery depending on the tomatoes used so you can add some tomato paste if it hasn't thickened enough here but that's a small detail.

Chicken

- 2 chicken breasts halved horizontally
- 2 large eggs
- 1 cup flour
- 1 tablespoon garlic powder
- 1/2 cup each of grated parmesan, normal/Italian breadcrumbs, and panko breadcrumbs
- 1/2 cup olive oil
- Mozzarella sliced or shredded
- Shredded parmesan

Prep:

1. Mix breadcrumbs, grated parmesan, and garlic powder together and put on a plate
2. Toss the flour into a separate bowl or plate
3. Crack eggs into another separate bowl and beat em
4. Get your cast Iron out at medium-high heat - add and bring the olive oil to temperature (350-400 degrees)
5. Set Oven to broil

Once sauce is made:

1. Halve chicken breasts horizontally so that you get 4 filets with each being roughly the same size
2. For each filet
    1. Turn filet around in the flour to coat each side
    2. Fully submerge filet in the egg solution so that there is no dry flour remaining on the outside
    3. Take filet and press into the bread crumbs, coating each side
    4. Set aside and repeat with other filets
3. Once finished you can cover and refrigerate them for a half hour+ to get the breading to stick better.  The grated parm in the crust here tends to help it stick well enough though so I don't bother.
4. Toss the breaded filets into the cast iron pan with the oil for roughly 3-4 minutes a side or "until golden brown"TM.  Before you flip the first time, sprinkle as much fresh parmesan on top as you like - the cast Iron + oil will turn it into a crispy wonderful crust
5. Once fried, put chicken aside on some paper towels and pour the oil out of the cast Iron, wipe it with a towel so its cleaned off, and then return it to low heat
6. Place chicken back in the pan, quickly ladle as much sauce as you want on each piece, cover each piece in the mozzarella, followed by parmesan and then throw the cast iron in your oven to broil about 3-4 minutes or until the cheese crispy
7. Wait to cool and then serve 


Ingredients
- .5 - 1 Bag of Carrots
- 1 Tbsp EVOO
- Salt and Pepper

For the Glaze:
- 2-3 Tbsp Honey
	- depends on desired sweetness - I tend to use just 2
- 3 Tbsp Tamari/Dark soy sauce
- 1.5 Tbsp of good quality Balsamic Vinegar
	- if you have cheap stuff I may add just a touch more
- .5-1 Tbsp citrus juice (Ive used lime, lemon and orange to good effect)

Cooking the carrots:
1. Preheat oven to 425
2. Peel carrots and cut into ~3-35. inch logs of fairly similar diameter
3. Toss carrots in EVOO, salt and pepper to taste
4. Spread evenly on a non-crowded tray
5. Into oven for 30 minutes
	1. Filp carrots after ~20-25 minutes.  This should mean that the carrots have an uneven cook but it builds up a layer of caremlization on the bottom which does great with our flavours here. 
	2. Keep an eye on them starting at 20 and flip when the bottoms look a medium-dark brown
6. Take out of the oven when done and place in a bowl awaiting the glaze

Glaze:
1. Add honey, tamari, balsamic vinegar to a small sauce pot - ~6 inches
2. Once carrots are flipped add pot to medium heat
3. Stir occasionally until the glaze begins to bubble
4. Once bubbling add citrus juice and continue to cook until bubbles begin to intensify
5. Cook for ~ 5 minutes once bubbles intensify on this heat level. 
	1. Bubbling continue to get more intense during this process, growing taller and larger bubbles
	2. If bubbles grow higher than say 1.5-2 inches while stirring remove from heat, continue stirring till they go down, and reapply heat
6. To judge done-ness, remove from heat, let bubbles settle while stirring and glaze should be about as thick as a processed maple syrup while warm. 
	1. If the sauce is too thick, add water and return to low-medium heat
	2. if too thin return to medium heat and continue to stirr for another minute.  repeat process as needed. 
	3. There is no hardening agent so the glaze will ramain sticky but also is reversable like this.  Use less heat after the initial cooking if needed to avoid fucking with the caramelization on our honey
7. Pour over carrots while hot
8. Flip carrots while the sauce cools, glaze should build up on the carrots and look uneven while still clinging well to the carrots
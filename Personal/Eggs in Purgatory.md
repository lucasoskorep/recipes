Ingredients:

- Olive oil
- 1 small-medium onion or 2 shallots diced
- 1-2 bell peppers, sliced, and then cut the slices to 3rds
- Oregano (medium)
- Tumeric (medium)
- Smoked Paprika(or just paprika) (light if smoked, medium if not)
- Cayanne pepper (light)
- Garlic Powder (heavy)
- teaspoon minced garlic
- salt
- pepper
- mozzerella cheese
- 1 15oz can diced fire roasted tomatoes
- 6 eggs
- Bread of choice

Steps:
1. Oven to broil
2. Add olive oil,  peppers and diced onion to medium heat for ~ 10 minutes
3. Place pan in the oven and broil peppers on high until the first edges of the peppers/onions are brown on top - roughly 2-5 minutes depending on how far your pan is from the broiler flames
4. Oven to 425
5. Add spices to taste
6. Add tomatoes to the pot, add salt if the tomatoes need it
7. Bring to a boil, then back down to a simmer to thicken.
8. Sprinkle some mozzarella on top right after you start simmering and mix it in
9. As the tomatoes soften, if there are any large ones just squish them with the back of a spoon
10. Once it is thicc enough to hold form, take a spoon and make 6 divots in the sauce
11. Crack eggs and put into divots
12. Place pan back in oven and bake for 8 minutes.
13. Toast some bread and spread olive oil on it.
14. Remove eggs+sauce from the oven and spoon over the bread
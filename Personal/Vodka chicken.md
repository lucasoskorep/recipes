Ingredients:

- 1 Diced tomato 14.5 oz
- 1 tomato fresh
- 1 can tomato paste
- 8-10 chicken tenders cut up or whatever chicken duh
- 4 cloves garlic + some powder
- 16 OZ of cream
- 1/3 cup vodka
- Cup mozzarella
- Half container Parmesan

Steps:

- Saute mushrooms in own pan
- 1 tomato + one can of diced tomatoes go into a pan with 8-10 tenders pre-cut in it
- saute for 20 minutes
- add mushroomstho
- add in cream + 1- 2 tomato pastes
- cook slowly on low for 5 minutes
- then add cheese
- go for another 5 minutes
- turn to medium
- add in red pepper flakes to cover the surface lightly - think pizza
- Add in italian seasoning, oregano, light amounts of basil and parsley
- Add in vodka
- Add in salt and pepper
- Stir and keep on heat for 5 minutes
- Turn down to lowest setting
- let broil for whatever you want

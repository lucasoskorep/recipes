* 1 yellow onion diced
* 2 cloves garlic (minced)
* 1 tablespoon olive oil
* 3 tablespoon curry powder
* 1.5 cup ketchup
* 2 teaspoon Worcestershire sauce
* 2 teaspoon paprika
* 3 tablespoon light brown sugar
* 1 teaspoon yellow mustard
* Pinch of cayenne
* curry powder for sprinkling

Steps:
- Pan on medium heat
- Fry onions in the oil until translucent
- Once translucent add garlic for 3 minutes until strongly fragrant
	- Try not to brown the onion but swapping the second the onion begins to brown is ideal
- Add in curry powder and stir for 30 seconds
- Add in the ketchup and all other ingredients
- Blend 
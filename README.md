# Recipes

This repo serves as a collection of recipes that I create or frequently use

I have broken it down into 2 subfolders
- Links 
	- frequently used recipes from other chefs
- Recipes
	- markdown of custom recipes I have made
